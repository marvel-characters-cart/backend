import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

import { MarvelCharacterCart } from '../../marvel-characters-cart/entity/marvel-character-cart.entity';
@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(type => MarvelCharacterCart, marveCharacterCart => marveCharacterCart.user)
  marvelCharacterCart: MarvelCharacterCart[];
}
