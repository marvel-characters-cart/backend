import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entity/user.entity';
import { USER_SEED } from './seed/user.seed';
import { MarvelCharacterCart } from '../marvel-characters-cart/entity/marvel-character-cart.entity';
import { MarvelCharactersCartService } from '../marvel-characters-cart/marvel-characters-cart.service';
import { AddCharacterToCartDto } from './dto/add-character-to-cart.dto';
import { UpdateCharacterToCartDto } from './dto/update-character-to-cart.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
    private readonly marvelCharactersCartService: MarvelCharactersCartService,
  ) { }

  /**
   * Returns all characters in the user's cart
   */
  async findMarvelCharactersCartByUserId(userId: number): Promise<MarvelCharacterCart[]> {
    return this.marvelCharactersCartService.findAllByUser(userId);
  }

  /**
   * Add to cart de character o update the quantity if already exist in cart.
   */
  async addMarvelCharacterCartByUserId(
    userId: number,
    addCharacterToCartDto: AddCharacterToCartDto,
  ): Promise<MarvelCharacterCart> {
    let marvelCharacterAdded: MarvelCharacterCart;

    const marvelCharacterInCartFound = await this.marvelCharactersCartService
      .findByUserIdAndMarvelCharacterId(userId, addCharacterToCartDto.marvelCharacterId);

    if (marvelCharacterInCartFound) {
      marvelCharacterAdded = await this.marvelCharactersCartService.updateQuantityById(
        marvelCharacterInCartFound.id, marvelCharacterInCartFound.quantity + 1,
      );
    } else {
      marvelCharacterAdded = await this.marvelCharactersCartService.create({
        ...addCharacterToCartDto,
        userId,
      });
    }

    return marvelCharacterAdded;
  }

  /**
   * Add characters to cart that incomes from the localStorage
   */
  async addCharactersToCartByUserId(
    userId: number,
    updateCharactersToCartDto: UpdateCharacterToCartDto[],
  ): Promise<MarvelCharacterCart[]> {
    const marvelCharactersUpdated: MarvelCharacterCart[] = [];

    for (const characterToAdd of updateCharactersToCartDto) {
      let marvelCharacterAdded: MarvelCharacterCart;
      const marvelCharacterInCartFound = await this.marvelCharactersCartService
        .findByUserIdAndMarvelCharacterId(userId, characterToAdd.marvelCharacterId);
      if (marvelCharacterInCartFound) {
        marvelCharacterAdded = await this.marvelCharactersCartService.updateQuantityById(
          marvelCharacterInCartFound.id,
          marvelCharacterInCartFound.quantity + characterToAdd.quantity,
        );
      } else {
        marvelCharacterAdded = await this.marvelCharactersCartService.create({
          ...characterToAdd,
          userId,
        });
      }
      marvelCharactersUpdated.push(marvelCharacterAdded);
    }
    return marvelCharactersUpdated;
  }

  /**
   * Search user by username return if exist; create if not.
   */
  async login(
    name: string,
  ): Promise<User> {
    let userLoged = await this.findByName(name);
    if (!userLoged) {
      const userToCreate = new User();
      userToCreate.name = name;
      userLoged = await this.usersRepository.save(userToCreate);
    }
    return userLoged;
  }

  async findByName(name: string): Promise<User> {
    return this.usersRepository.findOne({
      name,
    });
  }


  async seed(): Promise<any> {
    const userFound = await this.findByName(
      USER_SEED.name,
    );
    if (!userFound) {
      const userCreated = await this.usersRepository.save(USER_SEED);
      return userCreated;
    }
  }
}