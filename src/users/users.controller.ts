import { Controller, Get, Param, Post, Body, Query } from '@nestjs/common';
import { UsersService } from './users.service';
import { MarvelCharacterCart } from '../marvel-characters-cart/entity/marvel-character-cart.entity';
import { AddCharacterToCartDto } from './dto/add-character-to-cart.dto';
import { User } from './entity/user.entity';
import { UpdateCharacterToCartDto } from './dto/update-character-to-cart.dto';

@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) { }

  /**
   * Route that return all characters in user's cart
   */
  @Get(':userId/marvel-characters-cart')
  async findAllCharactersInCartByUserId(@Param('userId') userId: number): Promise<MarvelCharacterCart[]> {
    return await this.userService.findMarvelCharactersCartByUserId(userId);
  }

  /**
   * Route that add Character to user's cart
   */
  @Post(':userId/marvel-characters-cart')
  async addCharacterToCart(
    @Param('userId') userId: number,
    @Body() addCharacterToCartDto: AddCharacterToCartDto,
  ): Promise<MarvelCharacterCart> {
    return await this.userService.addMarvelCharacterCartByUserId(userId, addCharacterToCartDto);
  }

  /**
   * Route that add Characters to user's cart
   */
  @Post(':userId/add-marvel-characters-cart')
  async addCharactersToCart(
    @Param('userId') userId: number,
    @Body() updateCharacterToCartDto: UpdateCharacterToCartDto[],
  ): Promise<MarvelCharacterCart[]> {
    return await this.userService.addCharactersToCartByUserId(userId, updateCharacterToCartDto);
  }

  /**
   * Route login
   */
  @Get('/login')
  async login(
    @Query('name') name: string,
  ): Promise<User> {
    return await this.userService.login(name);
  }

}
