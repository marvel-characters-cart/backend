import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { MarvelCharactersCartModule } from '../marvel-characters-cart/marvel-characters-cart.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entity/user.entity';

describe('UsersController', () => {
  let controller: UsersController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot(),
        TypeOrmModule.forFeature([User]),
        MarvelCharactersCartModule,
      ],
      controllers: [UsersController],
      providers: [UsersService],
    })
    .compile();

    controller = module.get<UsersController>(UsersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
