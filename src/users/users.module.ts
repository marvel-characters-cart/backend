import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entity/user.entity';
import { MarvelCharactersCartModule } from '../marvel-characters-cart/marvel-characters-cart.module';

@Module({
  imports: [TypeOrmModule.forFeature([User]), MarvelCharactersCartModule],
  controllers: [UsersController],
  providers: [UsersService],
})
export class UsersModule {}
