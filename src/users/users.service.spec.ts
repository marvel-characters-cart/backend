import { Connection, Repository } from 'typeorm';
import { createMemDB } from '../utils/testing-helpers/createMemDb';
import { User } from '../users/entity/user.entity';
import { UsersService } from './users.service';
import { MarvelCharacterCart } from '../marvel-characters-cart/entity/marvel-character-cart.entity';
import { MarvelCharactersCartService } from '../marvel-characters-cart/marvel-characters-cart.service';

const MARVEL_CHARACTERS_CART_TEST_CASE = {
  userId: 1,
  marvelCharacterId: 1017100,
  marvelCharacterName: 'A-Bomb (HAS)',
  marvelCharacterDescription: "Rick Jones has been Hulk's best bud since day one, but now he's more than a friend...he's a teammate! Transformed by a Gamma energy explosion, A- Bomb's thick, armored skin is just as strong and powerful as it is blue. And when he curls into action, he uses it like a giant bowling ball of destruction!",
  marvelCharacterImageUrl: 'http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16.jpg',
};

const USER_TEST_CASE = { id: 1, name: 'user-test' };

describe('UsersService', () => {
  let db: Connection
  let usersRepository: Repository<User>;
  let usersService: UsersService;

  let marvelCharactersCartRepository: Repository<MarvelCharacterCart>;
  let marvelCharactersCartService: MarvelCharactersCartService;

  beforeAll(async () => {
    db = await createMemDB();
    await db.createQueryBuilder()
      .insert()
      .into(User)
      .values([USER_TEST_CASE])
      .execute();
    marvelCharactersCartRepository = db.getRepository(MarvelCharacterCart);
    marvelCharactersCartService = new MarvelCharactersCartService(marvelCharactersCartRepository);
    usersRepository = db.getRepository(User);
    usersService = new UsersService(usersRepository, marvelCharactersCartService);
  })
  afterAll(() => db.close())

  describe('addMarvelCharacterCartByUserId', () => {
    let marvelCharacterCart: MarvelCharacterCart;
    beforeEach(async () => {
      marvelCharacterCart = await usersService.addMarvelCharacterCartByUserId(
        USER_TEST_CASE.id,
        MARVEL_CHARACTERS_CART_TEST_CASE,
      );
    })

    it('must be create record if not exist ', async () => {
      expect(marvelCharacterCart).toBeDefined();
    });

    it('must be update quantity if already exist record', async () => {
      expect(marvelCharacterCart.quantity).toBe(2);
    });
  });

  describe('login', () => {    
    it('must be create user if not exist ', async () => {
      const userLoged = await usersService.login('goku');
      expect(userLoged).toBeDefined();
    });

    it('must be return user already exists', async () => {
      const userLoged = await usersService.login(USER_TEST_CASE.name);
      expect(userLoged).toBeDefined();
    });
  });

  describe('addCharactersToCartByUserId', () => {    
    it('must be add a character int cart if not exist ', async () => {
      const charactersInCart = await usersService.addCharactersToCartByUserId(1, [
        {
          marvelCharacterId: 1009171,
          marvelCharacterName: "Bastion",
          marvelCharacterDescription: "Description not found",
          marvelCharacterImageUrl: "http://i.annihil.us/u/prod/marvel/i/mg/d/80/52695253215f4.jpg",
          quantity: 5,
        },
      ]);
      expect(charactersInCart.length).toBeGreaterThan(0);
    });

    it('must be update character if already exists', async () => {
      await usersService.addCharactersToCartByUserId(1, [
        {
          marvelCharacterId: 1017100,
          marvelCharacterName: 'A-Bomb (HAS)',
          marvelCharacterDescription: "Rick Jones has been Hulk's best bud since day one, but now he's more than a friend...he's a teammate! Transformed by a Gamma energy explosion, A- Bomb's thick, armored skin is just as strong and powerful as it is blue. And when he curls into action, he uses it like a giant bowling ball of destruction!",
          marvelCharacterImageUrl: 'http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16.jpg',
          quantity: 3,
        }
      ]);
      const newCharacter = await marvelCharactersCartService.findByUserIdAndMarvelCharacterId(
        USER_TEST_CASE.id,
        MARVEL_CHARACTERS_CART_TEST_CASE.marvelCharacterId,
      )
      expect(newCharacter.quantity).toBe(5);
    });
  });
});
