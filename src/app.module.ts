import { Module } from '@nestjs/common';
import { MarvelCharactersCartModule } from './marvel-characters-cart/marvel-characters-cart.module';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    MarvelCharactersCartModule,
    UsersModule,
  ],
})
export class AppModule { }
