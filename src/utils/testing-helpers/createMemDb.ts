import { createConnection, EntitySchema } from 'typeorm'
type Entity = Function | string | EntitySchema<any>

export async function createMemDB() {
  return createConnection({
    type: 'postgres',
    host: "localhost",
    port: 5433,
    username: "postgres",
    password: "marvelCharactersCartTestPassword",
    database: "marvelCharactersCartTestDb",
    entities: [
      "dist/**/*.entity{.ts,.js}"
    ],
    synchronize: true,
    dropSchema: true,
  });
}
