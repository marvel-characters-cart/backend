import { Module } from '@nestjs/common';
import { MarvelCharactersCartController } from './marvel-characters-cart.controller';
import { MarvelCharactersCartService } from './marvel-characters-cart.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MarvelCharacterCart } from './entity/marvel-character-cart.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MarvelCharacterCart])],
  controllers: [MarvelCharactersCartController],
  providers: [MarvelCharactersCartService],
  exports: [MarvelCharactersCartService]
})
export class MarvelCharactersCartModule {}
