export class CreateMarvelCharacterCartDto {
  readonly userId: number;
  readonly marvelCharacterId: number;
  readonly marvelCharacterName: string;
  readonly marvelCharacterDescription: string;
  readonly marvelCharacterImageUrl: string;
}
