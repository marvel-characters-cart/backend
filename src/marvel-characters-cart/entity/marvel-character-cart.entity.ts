import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { User } from '../../users/entity/user.entity';

@Entity()
export class MarvelCharacterCart {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => User, user => user.marvelCharacterCart)
  user: User;

  @Column({
    default: 1,
  })
  quantity: number;

  @Column()
  marvelCharacterId: number;

  @Column()
  marvelCharacterName: string;

  @Column()
  marvelCharacterDescription: string;

  @Column()
  marvelCharacterImageUrl: string;
}
