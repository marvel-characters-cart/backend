import { MarvelCharactersCartService } from './marvel-characters-cart.service';
import { CreateMarvelCharacterCartDto } from './dto/create-marvel-character-cart.dto';
import { Connection, Repository, DeleteResult } from 'typeorm';
import { MarvelCharacterCart } from './entity/marvel-character-cart.entity';
import { createMemDB } from '../utils/testing-helpers/createMemDb';
import { User } from '../users/entity/user.entity';

const MARVEL_CHARACTERS_CART_TEST_CASE: CreateMarvelCharacterCartDto = {
  userId: 1,
  marvelCharacterId: 1017100,
  marvelCharacterName: 'A-Bomb (HAS)',
  marvelCharacterDescription: "Rick Jones has been Hulk's best bud since day one, but now he's more than a friend...he's a teammate! Transformed by a Gamma energy explosion, A- Bomb's thick, armored skin is just as strong and powerful as it is blue. And when he curls into action, he uses it like a giant bowling ball of destruction!",
  marvelCharacterImageUrl: 'http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16.jpg',
};

const USER_TEST_CASE = { id: 1, name: 'user-test' };


describe('MarvelCharactersCartService', () => {
  let db: Connection
  let marvelCharactersCartRepository: Repository<MarvelCharacterCart>;
  let marvelCharactersCartService: MarvelCharactersCartService;

  beforeAll(async () => {
    db = await createMemDB();
    await db.createQueryBuilder()
      .insert()
      .into(User)
      .values([USER_TEST_CASE])
      .execute();
    marvelCharactersCartRepository = db.getRepository(MarvelCharacterCart)
    marvelCharactersCartService = new MarvelCharactersCartService(marvelCharactersCartRepository) // <--- manually inject
  })
  afterAll(() => db.close())

  it('test if create a marvel Character Cart', async () => {
    const marvelCharactersCart = await marvelCharactersCartService.create(MARVEL_CHARACTERS_CART_TEST_CASE);
    expect(marvelCharactersCart).toBeDefined();
  });

  it('test findAllByUser return array with one marvelCharacterCart', async () => {
    const marvelCharactersCart: MarvelCharacterCart[] = await marvelCharactersCartService.findAllByUser(USER_TEST_CASE.id);
    expect(marvelCharactersCart.length).toBeGreaterThan(0);
  });

  it('test findByUserIdAndMarvelCharacterId return a marvelCharacterCart', async () => {
    const marvelCharactersCart: MarvelCharacterCart = 
      await marvelCharactersCartService
      .findByUserIdAndMarvelCharacterId(USER_TEST_CASE.id, MARVEL_CHARACTERS_CART_TEST_CASE.marvelCharacterId);
    expect(marvelCharactersCart).toBeDefined();
  });

  it('test updateQuantityById sum the quantity', async () => {
    const marvelCharactersCart: MarvelCharacterCart = 
      await marvelCharactersCartService.findByUserIdAndMarvelCharacterId(
        USER_TEST_CASE.id, MARVEL_CHARACTERS_CART_TEST_CASE.marvelCharacterId,
      );
    const marvelCharactersUpdated = await marvelCharactersCartService.updateQuantityById(
      marvelCharactersCart.id, 10,
    );    
    expect(marvelCharactersUpdated.quantity).toBe(10);
  });

  it('delete', async () => {
    const deleteResult: DeleteResult = await marvelCharactersCartService.delete(1);
    expect(deleteResult.affected).toBe(1);
  });
});
