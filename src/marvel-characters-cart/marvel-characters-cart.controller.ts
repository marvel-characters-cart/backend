import { Controller, Put, Delete, Param, Query } from '@nestjs/common';
import { MarvelCharactersCartService } from './marvel-characters-cart.service';
import { MarvelCharacterCart } from './entity/marvel-character-cart.entity';
import { DeleteResult } from 'typeorm';

@Controller('marvel-characters-cart')
export class MarvelCharactersCartController {
  constructor(private readonly marvelCharactersCartService: MarvelCharactersCartService) { }

  /**
   * Route that udpate quantity character in user's cart
  */
  @Put(':marvelCharacterCartId')
  async updateCharacterQuantityInCart(
    @Param('marvelCharacterCartId') marvelCharacterCartId: number,
    @Query('quantity') quantity: number,
  ): Promise<MarvelCharacterCart> {
    return await this.marvelCharactersCartService.updateQuantityById(
      marvelCharacterCartId, quantity
    );
  }

  /**
   * Route that delete character in user's cart
  */
  @Delete(':marvelCharacterCartId')
  async deleteCharacterInCart(
    @Param('marvelCharacterCartId') marvelCharacterCartId: number
  ): Promise<DeleteResult> {
    return await this.marvelCharactersCartService.delete(marvelCharacterCartId);
  }
}
