import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeleteResult } from 'typeorm'; 
import { MarvelCharacterCart } from './entity/marvel-character-cart.entity';
import { CreateMarvelCharacterCartDto } from './dto/create-marvel-character-cart.dto';
import { plainToClass } from 'class-transformer';
import { User } from '../users/entity/user.entity';

@Injectable()
export class MarvelCharactersCartService {
  constructor(
    @InjectRepository(MarvelCharacterCart)
    private readonly marvelCharacterCartRepository: Repository<MarvelCharacterCart>,
  ) { }

  /**
   * Return all marvel characters cart items; by userId.
   * @param userId - id of user
   */
  async findAllByUser(userId: number): Promise<MarvelCharacterCart[]> {
    const user = new User();
    user.id = userId;
    return this.marvelCharacterCartRepository.find({
      where: {
        user,
      }
    });
  }

  /**
   * Return marvel characters cart item; filter by userId and marvelCharacterId
   * @param userId - id of user
   * @param marvelCharacterId - id of marvel character that comes from marvel api
   */
  async findByUserIdAndMarvelCharacterId(
    userId: number, marvelCharacterId: number,
  ): Promise<MarvelCharacterCart> {
    const user = new User();
    user.id = userId;
    return this.marvelCharacterCartRepository.findOne({
      where: {
        user,
        marvelCharacterId,
      }
    });
  }

  /**
   * Create a record of MarvelCharacterCart
   * @param createCharacterToCartDto - object necessary for save record
   */
  async create(
    createCharacterToCartDto: CreateMarvelCharacterCartDto
  ): Promise<MarvelCharacterCart> {
    const user = new User();
    user.id = createCharacterToCartDto.userId;
    const marvelCharacterCart = plainToClass(MarvelCharacterCart, createCharacterToCartDto);
    marvelCharacterCart.user = user;
    return this.marvelCharacterCartRepository.save(marvelCharacterCart);
  }

  /**
   * Update the quantity specifying in the quantity param
   * @param marvelCharactersCartId - id of MarvelCharacterCart entity
   * @param quantity - the quantity to sum
   */
  async updateQuantityById(
    marvelCharactersCartId: number, quantity: number,
  ): Promise<MarvelCharacterCart> {
    const marvelCharacterCart = await this.marvelCharacterCartRepository.findOne({
      where: {
        id: marvelCharactersCartId,
      },
    });
    marvelCharacterCart.quantity = quantity;
    return this.marvelCharacterCartRepository.save(marvelCharacterCart);
  }

  /**
   * Delete a record
   * @param marvelCharactersCartId - id of MarvelCharacterCart entity
   */
  async delete(
    marvelCharactersCartId: number
  ): Promise<DeleteResult> {
    return this.marvelCharacterCartRepository.delete({
      id: marvelCharactersCartId
    });
  }
}