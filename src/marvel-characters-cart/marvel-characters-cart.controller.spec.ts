import { Test, TestingModule } from '@nestjs/testing';
import { MarvelCharactersCartController } from './marvel-characters-cart.controller';
import { MarvelCharactersCartService } from './marvel-characters-cart.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MarvelCharacterCart } from './entity/marvel-character-cart.entity';

describe('MarvelCharactersCartController', () => {
  let controller: MarvelCharactersCartController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot(),
        TypeOrmModule.forFeature([MarvelCharacterCart]),
      ],
      controllers: [MarvelCharactersCartController],
      providers: [MarvelCharactersCartService],
    }).compile();

    controller = module.get<MarvelCharactersCartController>(MarvelCharactersCartController);
  });
  describe('Metoditos', () => {
    it('should be defined', () => {
      expect(controller).toBeDefined();
    });
  });
});
